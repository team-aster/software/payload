#!/bin/sh
#Sleep is needed for serial port to be accessible (less time is also possible)
sleep 1
cd /home/pi/payload/flight
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
mkdir -p /home/pi/logs
python3 main.py >> /home/pi/logs/logs_from_main_$current_time.txt
if [ ! -f /boot/.no_reboot_on_exit ]; then
    sudo reboot
fi
