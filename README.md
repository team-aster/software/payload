# The ASTER experiment payload

This is the payload inside the Aster free-falling unit.
In the reduced configuration, it also contains the onboard data handling system.
It was successfully used during the flight on a REXUS rocket on the 21st March 2023.

# Installation

The payload runs on a Raspberry Pi. To run it automatically on reboot,
add the following line to the crontab (`sudo crontab -e`):
```
@reboot su pi -c "mkdir -p /home/pi/logs && sh /home/pi/payload/startup.sh >> /home/pi/logs/cronlog 2>&1"
```

