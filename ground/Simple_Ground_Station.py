import serial

read_in = input("Enter your serial port. Press RETURN to use the default COM6")
if read_in == '':
	com_port = 'COM6'
else:
	com_port = read_in

baud_rate = 38400
sync_byte = 0b01010101
bytes_in_message = 4

listener = serial.Serial(com_port, baud_rate, timeout = 2)
listener.reset_input_buffer()

output_buffer = []

while True:
	output_buffer.append(listener.read(listener.in_waiting or 1))
	if len(output_buffer) >= bytes_in_message:
		for e in output_buffer:
			if e == sync_byte:
				m = output_buffer.index(e)
				id = output_buffer[m+1:m+3]
				state = output_buffer[m+3]
				for n in range(m+5):
					output_buffer.pop(0)
				print(f"message received with id {id} transmitting state {state}")
				listener.write(id)