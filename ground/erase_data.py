#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 22:04:50 2021

@author: miguel

This script can be used to clean the Raspberry Pi from data from the tests.
It erases all videos, telemetry files, temperature monitoring csv and log files.
"""

import glob
import os
from main import MainPayloadProcess

def erase_files_in_directory(directory_path):
    # Given the directory absolute path, erase all the files inside it
    files = os.listdir(directory_path)
    for f in files:
        if not f == 'temperature.csv':
            os.remove(os.path.join(directory_path, f))

def erase_data_from_csv(csv_filename):
    # Given a filename (absolute path) of a csv, erase everything inside (truncate)
    f = open(csv_filename, "w+")
    f.close()

        
answer = input(' *** Warning!! *** \nThis execution deletes all the saved data, including videos, telemetry backup data, temperature sensor log and some execution logs.\n\nAre you sure that you want to delete all this data?  [Yes/No]\n')
if answer.lower() in ["yes"]:
     print('Erase confirmed. Deleting files')
     erase = True
elif answer.lower() in ["n","no"]:
     print('Aborting to erase files. No files will be deleted ')
     erase = False
else:
     print('Invalid answer, aborting. No files will be deleted')
     erase = False
     

     
if erase:  
    print('...')
    
    main = MainPayloadProcess()
    
    temperature_csv = main.temperature_monitoring_csv_path 
    video_path = main.recorder.path_to_save_videos 
    telemetry_path = main.path_to_save_telemetry 
    logs_path = '/home/pi/logs' 

    erase_files_in_directory(video_path)
    erase_files_in_directory(telemetry_path)
    erase_files_in_directory(logs_path)
    erase_data_from_csv(temperature_csv)
 
    
    print('All files erased') 
     




