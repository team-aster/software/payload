#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script can be used to merge video files from the Raspberry Pi into a .mp4 file.
It requires to have the program ffmpeg working (If using windows download from
https://ffmpeg.org/download.html and follow a tutorial like this one
 https://video.stackexchange.com/questions/20495/how-do-i-set-up-and-use-ffmpeg-in-windows)

The path where the video files are stored must be defined in the parameters section below.

Give the date in the filename of the files you want to convert (in variable date_in_filename)

If you want to convert all files in directory (and merge with the corresponding batch of files),
set the parameter process_all_files as True.
"""

import os
import subprocess
import numpy as np

###############################
""" PARAMETERS TO BE DEFINED BY USER """
# Define path where files are stored

default_videos_directory_path = os.path.join(os.sep, 'home', 'pi', 'Videos')
rel_path = input(f'Enter the directory of videos you want to look in [default={default_videos_directory_path}]')
videos_directory_path = os.path.abspath(os.path.join(os.getcwd(), rel_path)) if rel_path else default_videos_directory_path

# Give common part (date) of video that you want to merge and convert (i.e. 20211116-192855)
default_date_in_filenames = '20211116-192855'
date_in_filenames = input(f'enter the date/timestamp of the sequence you want to convert [default={default_date_in_filenames}]') or default_date_in_filenames

# If True, it merges all the video files in the folder by batches
# (date_in_filenames variable is ignored)
process_all_files = False


################################


def merge_and_convert_file_batch(date_in_filenames, allfiles):
    # Select only the files that correspond to the given date
    files_to_convert = [x for x in allfiles if (date_in_filenames in x)]

    # This next command is useful to perform human sorting. Not needed with new filesystem,
    # but with the old way of naming RPi files.
    files_to_convert = sorted(files_to_convert, key=sort_numerically)
    # Write .txt file with list of files to merge and convert
    with open(os.path.join(videos_directory_path,
                           "files_to_convert_{}.txt".format(date_in_filenames)), "w+") as textfile:
        for file in files_to_convert:
            textfile.write('file {} \n'.format(os.path.join(videos_directory_path, file)))

    # Run ffmpeg
    print("Running ffmpeg for file batch with date " + date_in_filenames[5:])
    subprocess.call(['ffmpeg', '-f', 'concat', '-i',
                     os.path.join(videos_directory_path,
                                  "files_to_convert_{}.txt".format(date_in_filenames)),
                     '-c', 'copy',
                     os.path.join(videos_directory_path,
                                  "merged_{}.mp4".format(date_in_filenames))])


def sort_numerically(filename):
    """ Function that helps to perform human sorting of list of filenames"""
    return int(filename.split('_')[-1][:-5])


########################################################################

def main():
    # Get all .h264 files in directory .
    allfiles = [f for f in os.listdir(videos_directory_path) if f.endswith(".h264")]

    if process_all_files:

        dates_in_filenames = np.unique([f.split('_')[-2] for f in allfiles])
        for dates in dates_in_filenames:
            merge_and_convert_file_batch(dates, allfiles)
    else:
        merge_and_convert_file_batch(date_in_filenames, allfiles)


if __name__ == '__main__':
    main()
