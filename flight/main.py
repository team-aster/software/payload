#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import time
import logging
import subprocess

# Importing and running multiple SPI device fix for the two ADCs
import ADC_SPI_fix

# Importing all payload packages
import REXUS
from gopro import GoPro

from log import initLogging, telemetryHandler
from config import Config
from timeutils import sleepUntil
from threads.video import Recorder
from threads.telemetry import TelemetryThread
from threads.continuity import ContinuityInst
from threads.morseBlink import MorseBlinkInst
from threads.telecommands import TelecommandThread
from threads.reactionWheel import ReactionWheelThread
from threads.cpuTemperatureReader import CpuTemperatureReaderThread

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))

    from ecom.checksum import ChecksumVerifier
    from ecom.database import CommunicationDatabase
    from ecom.datatypes import EnumType
    from ecom.serializer import TelemetrySerializer

except ImportError:
    raise ImportError('Shared communication database module missing, '
                      'run "git submodule update --init --recursive"')


class SystemState(EnumType):
    """ The states of the main state machine. """

    ERROR = 0
    INIT = 1
    IDLE = 2
    RECORDING = 3
    TESTING_REACTION_WHEELS = 4


class MainPayloadProcess:
    def __init__(self):
        initLogging()
        self._database = CommunicationDatabase(os.path.join(os.path.dirname(__file__), 'communication'))
        self._database.replaceType(SystemState)

        # Start of initialization sequence #
        # Load all configuration data
        self._config = Config.load(self._database)

        # Initialize telemetry
        rxsm = REXUS.Rxsm(self._database)
        self.telemetryThread = TelemetryThread(self._database, self._config, rxsm)
        telemetryHandler.setTelemetry(self._database, self.telemetryThread)
        self.logger = logging.getLogger(__name__)

        # CPU temperature monitoring initialization
        self.cpuTemperatureReaderThread = CpuTemperatureReaderThread(self._config)

        # PiCam initialization
        self.picam = Recorder(self._config)

        # GoPro initialisation
        self._goPro = GoPro(self._config)

        # REXUS signals initialization
        self.REXUSProcess = REXUS.REXUSInst(self._config)

        # RMU-FFU interface continuity test initialization
        self.continuityThread = ContinuityInst(self._config)

        # Reaction wheel initialization
        self.reactionWheelThread = ReactionWheelThread(self._config)

        # Morse blink LED initialization
        self.morseBlinkThread = MorseBlinkInst(self._config)

        self.telecommandThread = TelecommandThread(self._database, self._config, rxsm)
        self.telemetryThread.setDataThreads(
            self.cpuTemperatureReaderThread, self.continuityThread, self.reactionWheelThread, self.picam)

        # Main loop state machine
        self.state = SystemState.INIT

    def main(self):
        if self.state is SystemState.INIT:
            self.state = SystemState.IDLE
            self.telemetryThread.start()
            self.telecommandThread.start()
        else:
            self.state = SystemState.ERROR
            self.logger.info('Main loop in error in state. Initial state was not "INIT"')
        try:
            git_rev = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])\
                .decode('utf-8', errors='ignore').strip()
        except subprocess.CalledProcessError as error:
            self.logger.warning(f'Failed to query git revision: {error}')
            git_rev = 'unknown'
        self.logger.info(f'Software git revision: {git_rev}')
        try:
            os_stats = os.statvfs('/')
            self.logger.info(f'Remaining space on SD card: {os_stats.f_bavail * os_stats.f_frsize}')
        except IOError as error:
            self.logger.warning(f'Failed to get the remaining space on the SD card: {error}')

        while True:
            start = time.time()
            self.telemetryThread.sendHeartbeat(self.state)

            loValue, sodsValue, soeValue = self.REXUSProcess.REXUS_Proc()
            if self._config.enableVerboseLogging:
                self.logger.debug(f'LO = {loValue}, SODS = {sodsValue}, SOE = {soeValue}')
            if self.state is SystemState.IDLE:
                if self.telecommandThread.haveResetRequest:
                    self.logger.info(f'Rebooting at {time.time()} due to telecommand')
                    break
                if sodsValue:
                    self.state = SystemState.RECORDING
                    self.logger.debug(f'Switching state to {self.state.name}')
                    self.picam.start()
                    self.cpuTemperatureReaderThread.start()
                    self.continuityThread.start()
                    self._goPro.activate()
                    self.morseBlinkThread.start()
            elif self.state is SystemState.RECORDING:
                if sodsValue and soeValue:
                    self.state = SystemState.TESTING_REACTION_WHEELS
                    self.logger.debug(f'Switching state to {self.state.name}')
                    self.reactionWheelThread.start()
                elif not sodsValue:
                    self.state = SystemState.IDLE
                    self.logger.debug(f'Switching state to {self.state.name}')
                    self.logger.info('Stopping PiCam, can take <5s')
                    # TODO: Find a way to make stopping the PiCam not block for 5 seconds
                    self.picam.stop()
                    self.cpuTemperatureReaderThread.stop()
                    self.continuityThread.stop()
                    self.logger.debug('Stopping GoPro')
                    self._goPro.deactivate()
                    self.morseBlinkThread.stop()
            elif self.state is SystemState.TESTING_REACTION_WHEELS:
                if (not soeValue) or (not sodsValue):
                    self.state = SystemState.RECORDING
                    self.logger.debug(f'Switching state to {self.state.name}')
                    self.reactionWheelThread.stop()
            elif self.state is SystemState.ERROR:
                self.logger.info(f'Rebooting at {time.time()} due to error state')
                os.system('sudo shutdown -r now')
            else:
                self.logger.error(f'Unknown state {self.state!r}, switching to error state')
                self.state = SystemState.ERROR
            self._goPro.update()
            sleepUntil(start + self._config.mainThreadFrequency)

        self.telemetryThread.stop()
        self.telecommandThread.stop()
        self.morseBlinkThread.stop()
        self.reactionWheelThread.stop()
        self.continuityThread.stop()
        self.picam.stop()
        self.cpuTemperatureReaderThread.stop()


if __name__ == '__main__':
    MainPayloadProcess().main()
