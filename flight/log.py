import os
import sys
import logging

from threading import RLock, Lock

from timeutils import getSystemTime

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.database import CommunicationDatabase
except ImportError:
    raise ImportError('Shared communication database module missing, run "git submodule update --init --recursive"')


class SystemTimeLogRecord(logging.LogRecord):
    """ A log record that includes the system time. """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__.update({'systemTime': getSystemTime()})


class TelemetryHandler(logging.Handler):
    """ A logging handler that sends logs as telemetry messages. """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._telemetry = None
        self._levelMapping = {}
        self._defaultLevel = None
        self._recursiveLock = RLock()
        self._lock = Lock()

    def setTelemetry(self, database, telemetry):
        """
        Initialize the handler with the telemetry system.

        :param database: The telemetry database.
        :param telemetry: The telemetry sender.
        """
        messageType = database.dataTypes['MessageType'].type
        self._levelMapping = {
            logging.CRITICAL: messageType['CRITICAL_ERROR_MESSAGE'],
            logging.ERROR: messageType['ERROR_MESSAGE'],
            logging.WARNING: messageType['WARNING_MESSAGE'],
            logging.INFO: messageType['INFO_MESSAGE'],
            logging.DEBUG: messageType['DEBUG_MESSAGE'],
        }
        self._defaultLevel = messageType['DEBUG_MESSAGE']
        self._telemetry = telemetry

    def emit(self, record: logging.LogRecord):
        if self._telemetry is None:
            return
        with self._recursiveLock:
            if not self._lock.acquire(blocking=False):
                # We are trying to emit a log while emitting another log.
                # This can happen if an error log is generated while writing another log.
                return
            # noinspection PyBroadException
            try:
                message = self.format(record)
                self._telemetry.sendMessage(self._levelMapping.get(record.levelno, self._defaultLevel), message)
            except Exception:
                self.handleError(record)
            finally:
                self._lock.release()


telemetryHandler = TelemetryHandler()


def initLogging():
    """ Initialize the logging system. """
    logging.setLogRecordFactory(SystemTimeLogRecord)

    # noinspection SpellCheckingInspection
    formatter = logging.Formatter(
        fmt='%(created)s (%(systemTime)s) %(levelname)-8s %(name)s:%(funcName)s: %(message)s')

    # Configure logging to console
    stdoutHandler = logging.StreamHandler(sys.stdout)
    stdoutHandler.setLevel(logging.DEBUG)
    stdoutHandler.setFormatter(formatter)
    # additional handler for logging exceptions
    stderrHandler = logging.StreamHandler(sys.stderr)
    stderrHandler.setLevel(logging.ERROR)
    stderrHandler.setFormatter(formatter)

    # Create logger
    logging.root.setLevel(logging.DEBUG)
    # Add the handlers to the logger
    logging.root.addHandler(stdoutHandler)
    logging.root.addHandler(stderrHandler)
    logging.root.addHandler(telemetryHandler)
