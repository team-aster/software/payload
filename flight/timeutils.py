from time import sleep, time


_startupTime = time()
""" The time the system has started. """


def getSystemTime() -> float:
    """
    :return: The time since the system start in seconds.
    """
    return time() - _startupTime


def sleepUntil(absoluteTimeInSeconds: float, sleepFunction=sleep):
    """
    Suspend the current thread until the absolute time has passed.

    :param absoluteTimeInSeconds: An absolute timestamp in the system time in seconds.
    :param sleepFunction: The function that is used to suspend the thread.
    """
    try:
        sleepFunction(max(0.0, absoluteTimeInSeconds - time()))
    except TimeoutError:
        pass
