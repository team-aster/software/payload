from time import time

from gpiozero import DigitalOutputDevice

from config import Config


class GoPro:
    """ A handler for a GoPro camera. """
    def __init__(self, config: Config):
        """
        Initialize a new GoPro handler.

        :param config: The system configuration.
        """
        super().__init__()
        self._config = config
        self._triggerPin = DigitalOutputDevice(self._config.goProTriggerPin, initial_value=False)
        self._activated = False
        self._nextTriggeredOffTime = None

    def update(self):
        """ Update the internal state of the GoPro. This should be called at least once per second. """
        if self._nextTriggeredOffTime is not None and time() > self._nextTriggeredOffTime:
            self._nextTriggeredOffTime = None
            self._triggerPin.off()

    def activate(self):
        """ Activate the GoPro by sending a HIGH signal for config.goProActivationPressDuration seconds. """
        if not self._config.goProEnabled or self._activated:
            return
        self._activated = True
        self._triggerPin.on()
        self._nextTriggeredOffTime = time() + self._config.goProActivationPressDuration

    def deactivate(self):
        """ Deactivate the GoPro by sending a HIGH signal for config.goProDeactivationPressDuration seconds. """
        if not self._config.goProEnabled or not self._activated:
            return
        self._activated = False
        self._triggerPin.on()
        self._nextTriggeredOffTime = time() + self._config.goProDeactivationPressDuration
