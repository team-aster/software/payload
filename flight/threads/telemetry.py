import os
import sys

from time import time

from REXUS import Rxsm
from config import Config
from timeutils import getSystemTime, sleepUntil
from threadutils import RestartableThread

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.database import CommunicationDatabase
except ImportError:
    raise ImportError('Shared communication database module missing, run "git submodule update --init --recursive"')


class TelemetryThread(RestartableThread):
    """ A thread that handles sending telemetry via the RXSM interface to the groundstation. """

    def __init__(self, database: CommunicationDatabase, config: Config, rxsm: Rxsm):
        """
        Initialize the telemetry thread.

        :param database: The communication database.
        :param config: The system configuration.
        :param rxsm: The RXSM connection.
        """
        super().__init__()
        self._config = config
        self._rxsm = rxsm
        self._DATA_TELEMETRY = database.getTelemetryByName('DATA')
        self._MESSAGE_TELEMETRY = database.getTelemetryByName('MESSAGE')
        self._HEARTBEAT_TELEMETRY = database.getTelemetryByName('HEARTBEAT')
        self._CAMERA_DATA_TELEMETRY = database.getTelemetryByName('CAMERA_DATA')
        messageType = database.dataTypes['MessageType'].type
        self._DEBUG_MESSAGE = messageType['DEBUG_MESSAGE']
        self._lastHeartbeatTime = 0
        self._dataThreads = None

    def run(self):
        """ Send telemetry in a fixed interval until stopped. """
        try:
            self._stopEvent.wait(0.5)  # Wait a bit before gathering and sending the first data.
        except TimeoutError:
            pass
        while not self._stopEvent.is_set():
            tick = time()
            if self._dataThreads is not None:
                cpuTemperatureThread, continuityThread, reactionWheelThread, videoThread = self._dataThreads
                self._rxsm.sendTelemetry(self._DATA_TELEMETRY, {
                    'time': round(getSystemTime() * 1000),
                    'processor temperature': cpuTemperatureThread.currentTemperature,
                    'interface continuity': continuityThread.currentResistance,
                    'motor current': reactionWheelThread.currentWheelCurrent,
                    'motor speed': reactionWheelThread.currentWheelSpeed,
                    'target motor speed': reactionWheelThread.currentWheelSpeedTarget,
                    'reaction wheel controller state':
                        reactionWheelThread.rampIndex if reactionWheelThread.running else 255,
                })
                if not self._config.flightMode:
                    recordingDuration = videoThread.currentRecordingDuration
                    self._rxsm.sendTelemetry(self._CAMERA_DATA_TELEMETRY, {
                        'time': round(getSystemTime() * 1000),
                        'duration': 0 if recordingDuration is None else round(recordingDuration * 1000),
                        'number of chunks written': videoThread.chunksWritten,
                    })
            sleepUntil(tick + self._config.telemetrySendInterval)

    def sendDebugMessage(self, message: str):
        """
        Send a debugging message.

        :param message: The message to send.
        """
        self.sendMessage(self._DEBUG_MESSAGE, message)

    def sendMessage(self, level, message: str):
        """
        Send a message.

        :param level: The level of the message to send.
        :param message: The message to send.
        """
        if level is self._DEBUG_MESSAGE and self._config.flightMode:
            return
        message_bytes = message.encode('utf-8', errors='replace')
        self._rxsm.sendTelemetry(self._MESSAGE_TELEMETRY, {
            'time': round(getSystemTime() * 1000),
            'type': level,
            'size': len(message_bytes),
            'message': message_bytes,
        })

    def sendHeartbeat(self, systemState):
        """
        Send a heartbeat, if enough time has passed since the last heartbeat was sent.

        :param systemState: The current state of the system.
        """
        now = time()
        if now - self._lastHeartbeatTime > self._config.heartbeatSendInterval:
            self._rxsm.sendTelemetry(self._HEARTBEAT_TELEMETRY, {
                'time': round(getSystemTime() * 1000),
                'state': systemState,
            })
            self._lastHeartbeatTime = now

    def setDataThreads(self, *threads):
        """
        Register the threads that generate telemetry data.

        :param threads: A list of threads that generate telemetry data.
        """
        self._dataThreads = threads
