import os
import sys
import logging

from time import time

import gpiozero

from config import Config, ampere, rpm, SetPoint

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.database import CommunicationDatabase
    from ecom.datatypes import EnumType
except ImportError:
    raise ImportError('Shared communication database module missing, run "git submodule update --init --recursive"')
from timeutils import sleepUntil
from threadutils import RestartableThread


class ReactionWheelThread(RestartableThread):
    """ A thread that controls the reaction wheels and executes a ramp. """

    MOTOR_CONTROLLER_MAX_CURRENT_DRAW: ampere = 1
    """ The maximum current drawn by the motor. """

    def __init__(self, config: Config):
        super().__init__()
        self._config = config
        self._wheelCurrent = 0
        self._wheelSpeed = 0
        self._rampIndex = 0

        # Initialize Enable Pin
        self.EnablePin = gpiozero.DigitalOutputDevice(self._config.reactionWheelEnablePin)
        self.EnablePin.off()
        logging.debug(f'Reaction Wheel Enable Pin initialized as output on pin {self._config.reactionWheelEnablePin}')

        # Initialize PWM Control Pin
        self.SpdCtlPWM = gpiozero.PWMOutputDevice(
            self._config.reactionWheelSpeedControlPin, frequency=self._config.reactionWheelSpeedControlPinFrequency)
        logging.debug(f'Reaction Wheel Speed Control Pin initialized as output '
                      f'on pin {self._config.reactionWheelSpeedControlPin}')
        # Initialize the PWM Frequency
        logging.debug(f'Reaction Wheel Speed Control PWM frequency set '
                      f'to {self._config.reactionWheelSpeedControlPinFrequency} Hz')
        self.SpdCtlPWM.off()

        # Initialize Speed Feedback Pin
        self.SpdFdBckPin = gpiozero.MCP3008(channel=self._config.reactionWheelSpeedFeedbackAdcChannel,
                                            device=self._config.reactionWheelSpeedFeedbackAdcDevice)
        logging.debug(f'Reaction Wheel Speed Feedback Channel initialized '
                      f'as channel {self._config.reactionWheelSpeedFeedbackAdcChannel} '
                      f'on ADC device {self._config.reactionWheelSpeedFeedbackAdcDevice}')

        # Initialize Current Feedback Pin
        self.CurFdBck = gpiozero.MCP3008(channel=self._config.reactionWheelCurrentFeedbackAdcChannel,
                                         device=self._config.reactionWheelCurrentFeedbackAdcDevice)
        logging.debug(f'Reaction Wheel Current Feedback Channel initialized '
                      f'as channel {self._config.reactionWheelCurrentFeedbackAdcChannel} '
                      f'on ADC device {self._config.reactionWheelCurrentFeedbackAdcDevice}')

    @property
    def rampIndex(self) -> int:
        """
        :return: The current index into the ramp.
        """
        return self._rampIndex

    @property
    def currentWheelCurrent(self) -> ampere:
        """
        :return: The current ampere used by the reaction wheel.
        """
        return self._wheelCurrent

    @property
    def currentWheelSpeed(self) -> rpm:
        """
        :return: The current speed of the reaction wheel in rpm.
        """
        return self._wheelSpeed

    @property
    def currentRampSetPoint(self) -> SetPoint:
        """
        :return: The current set point of the reaction wheel ramp.
        """
        ramp = self._config.reactionWheelRamp
        rampIndex = self._rampIndex
        if rampIndex >= len(ramp):
            rampIndex = len(ramp) - 1
        return ramp[rampIndex]

    @property
    def currentWheelSpeedTarget(self) -> rpm:
        """
        :return: The current targeted speed of the reaction wheel in rpm.
        """
        return self._dutyCycleToRpm(self.currentRampSetPoint.targetDutyCycle / 255)

    def run(self):
        currentRampStateStart = time()
        self._rampIndex = 0
        if not self._config.reactionWheelEnabled:
            self.SpdCtlPWM.value = self._uint8ToDutyCycle(0)
            self.SpdCtlPWM.off()
            logging.info(f'Reaction Wheel 1 Test Not Active (reactionWheelEnabled = False)')
            return
        self.SpdCtlPWM.value = self._uint8ToDutyCycle(self.currentRampSetPoint.targetDutyCycle)
        self.EnablePin.on()
        while True:
            tick = time()
            speedVoltage = self.SpdFdBckPin.voltage
            currentVoltage = self.CurFdBck.voltage
            if self._config.enableVerboseLogging:
                logging.debug(f'Speed Feedback Pin Voltage = {speedVoltage}, Ramp index = {self._rampIndex}')
                logging.debug(f'Current Feedback Pin Voltage = {currentVoltage}')
            self._wheelSpeed = (speedVoltage / self.SpdFdBckPin.max_voltage) * self._config.reactionWheelMaxSpeed
            self._wheelCurrent = (currentVoltage / self.CurFdBck.max_voltage) * self.MOTOR_CONTROLLER_MAX_CURRENT_DRAW
            if self._stopEvent.is_set():
                # Shutdown the motor from any point in it's expected test
                self.SpdCtlPWM.value = self._uint8ToDutyCycle(0)
                self.EnablePin.off()
                self._rampIndex = 0
                self._wheelSpeed = 0
                self._wheelCurrent = 0
                return
            if self._rampIndex < len(self._config.reactionWheelRamp):
                setPoint = self.currentRampSetPoint
                if time() > currentRampStateStart + setPoint.deltaTime:
                    currentRampStateStart = time()
                    self._rampIndex += 1
                    if self._rampIndex >= len(self._config.reactionWheelRamp):
                        self.EnablePin.off()
                    setPoint = self.currentRampSetPoint
                self.SpdCtlPWM.value = self._uint8ToDutyCycle(setPoint.targetDutyCycle)
            sleepUntil(tick + self._config.reactionWheelThreadFrequency, sleepFunction=self._stopEvent.wait)

    def _dutyCycleToRpm(self, dutyCycle: float) -> rpm:
        """
        :param dutyCycle: A duty cycle. (0 - 1)
        :return: The rpm speed.
        """
        return self._config.reactionWheelMaxSpeed * dutyCycle

    @staticmethod
    def _uint8ToDutyCycle(speed: int) -> float:
        """
        Convert a 'uint8' to a duty cycle.
        The motor controller must not revive values between 0 - 0.1 and 0.9 and 1.

        :param speed: The speed represented as a 'uint8' value.
        :return: The duty cycle percentage corresponding to the value.
        """
        MARGIN = 0.09
        return MARGIN + (1 - 2 * MARGIN) / 255 * speed


if __name__ == "__main__":
    import sys
    import RPi.GPIO as GPIO  # Unit test will not execute as ReactionWheelThread does not support RPi.GPIO anymore
    from ecom.database import CommunicationDatabase

    GPIO.setmode(GPIO.BOARD)
    try:
        ReactionWheelThread(Config.load(CommunicationDatabase('communication'))).run()
    except:
        print('ReactionWheel: Unit test will not execute as ReactionWheelThread does not support RPi.GPIO anymore. '
              'Aborted')
        sys.exit()
