import os
import itertools

from time import time, strftime
from typing import Optional

import picamera

from config import Config
from threadutils import RestartableThread


class Recorder(RestartableThread):
    def __init__(self, config: Config):
        super().__init__()
        self._config = config
        self.path_to_save_videos = '/home/pi/Videos/'
        try:
            os.makedirs(self.path_to_save_videos, exist_ok=True)
        except IOError as error:
            self._logger.error(f'Failed to ensure the video save path at {self.path_to_save_videos}, '
                               f'saving videos might not work: {error}')
        self._recordingStartTime = None
        self._chunksWritten = 0

    @property
    def currentRecordingDuration(self) -> Optional[float]:
        """
        :return: The duration in seconds of the current recording session or None if not recording.
        """
        return None if self._recordingStartTime is None else time() - self._recordingStartTime

    @property
    def chunksWritten(self) -> int:
        """
        :return: The number of video chunks that have been written to the SD-Card in the current recording session.
        """
        return self._chunksWritten

    def run(self):
        """
        Records over multiple files without specifying the total duration beforehand.
        It starts recording when input StopRecordEvent (Type: threading.Event) is set.
        """
        if not self._config.videoSystemEnabled:
            return
        self._recordingStartTime = time()
        self._chunksWritten = 0
        formattedTime = strftime("%Y%m%d-%H%M%S")
        with picamera.PiCamera() as camera:
            camera.resolution = (self._config.videoResolution.width, self._config.videoResolution.height)
            camera.framerate = self._config.videoFps
            for i, _ in enumerate(camera.record_sequence(self._filenames(formattedTime))):
                camera.wait_recording(self._config.videoChunkLength)
                self._chunksWritten = i + 1
                if self._stopEvent.is_set():
                    break
        self._recordingStartTime = None
        self._chunksWritten = 0

    def _filenames(self, formattedTime: str):
        """
        Generate series of filenames from formattedTime and counter.
        :param formattedTime: The time when the recording started as a formatted string.
        :return: A generator that will produce valid video file names indefinitely.
        """
        for i in itertools.count(1):
            filename = os.path.join(self.path_to_save_videos, f'video{formattedTime}_{i:>04}.h264')
            if not os.path.exists(filename):
                yield open(filename, 'wb')


if __name__ == '__main__':
    print("TODO: build unit test for video recorder")
