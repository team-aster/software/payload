import os
import time

from gpiozero import MCP3008

from config import Config
from timeutils import sleepUntil
from threadutils import RestartableThread


class ContinuityInst(RestartableThread):
    def __init__(self, config: Config):
        super().__init__()
        self._config = config
        comparingResistance = config.continuityComparingResistance
        self._currentLimitingResistance = config.continuityCurrentLimitingResistance
        self._currentResistance = None
        maxVoltage = comparingResistance / (comparingResistance + self._currentLimitingResistance) * 3.3
        self.ContinuityPin = MCP3008(
            channel=self._config.continuityAdcChannel, device=self._config.continuityAdcDevice, max_voltage=maxVoltage)
        self._logger.info(f'Continuity: SPI initialized.')
        self._logger.info(f'Max Voltage = {self.ContinuityPin.max_voltage} V')
        self._logger.info(f'Bit Resolution = {self.ContinuityPin.bits}')
        self._logger.info(f'Channel = {self.ContinuityPin.channel}')
        self._readResistance()

    @property
    def currentResistance(self) -> float:
        """
        :return: The current resistance on the RMU - FFU interface.
        """
        return self._currentResistance

    def run(self):
        logPath = self._config.continuityLogPath.rstrip(b'\x00').decode('utf-8', errors='ignore')
        try:
            os.makedirs(os.path.dirname(logPath), exist_ok=True)
        except IOError as error:
            self._logger.error(f'Failed to ensure the continuity data save path at {logPath}, '
                               f'saving continuity data might not work: {error}')
        while not self._stopEvent.is_set():
            startTimeLoc = time.time()
            with open(logPath, 'a') as f:
                voltage, resistance = self._readResistance()
                f.write(f"{time.time()} - {voltage} (V) - {resistance} (Ohm)\n")
            sleepUntil(startTimeLoc + self._config.continuityThreadFrequency, sleepFunction=self._stopEvent.wait)

    def _readResistance(self):
        """ Read the current resistance on the RMU - FFU interface. """
        voltage = self.ContinuityPin.voltage
        # Convert the measured voltage to resistance of the interface
        self._currentResistance = 0 if voltage == 0 else self._currentLimitingResistance / (3.3 / voltage - 1)
        return voltage, self._currentResistance
