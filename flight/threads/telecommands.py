import os
import shutil
import sys
import subprocess

from enum import Enum
from typing import Any

from REXUS import Rxsm
from config import Config
from threadutils import RestartableThread

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.database import CommunicationDatabase
except ImportError:
    raise ImportError('Shared communication database module missing, run "git submodule update --init --recursive"')


class TelecommandThread(RestartableThread):
    """ A thread that parses and handles incoming telecommands from the RXSM interface. """

    def __init__(self, database: CommunicationDatabase, config: Config, rxsm: Rxsm):
        """
        Initialize the telecommand thread.

        :param database: The communication database.
        :param config: The system configuration.
        :param rxsm: The RXSM connection.
        """
        super().__init__()
        self._database = database
        self._config = config
        self._rxsm = rxsm
        self._haveResetRequest = False
        telecommandTypeEnum = database.telecommandTypeEnum
        self._telecommandHandlers = {
            telecommandTypeEnum['GET_STORAGE_SPACE']: self._handleGetStorageSpace,
            telecommandTypeEnum['SET_FLIGHT']: self._handleSetFlight,
            telecommandTypeEnum['GET_FLIGHT']: self._handleGetFlight,
            telecommandTypeEnum['SET_CONFIG']: self._handleSetConfig,
            telecommandTypeEnum['GET_CONFIG']: self._handleGetConfig,
            telecommandTypeEnum['CLEAR_LOGS']: self._handleClearLogs,
            telecommandTypeEnum['RESET_CHIP']: self._handleResetChip,
            telecommandTypeEnum['SEND_COMMAND']: self._handleSendCommand,
        }
        self._RESET_CHIP_VERIFICATION_CODE = self._database.constants['RESET_CHIP_VERIFICATION_CODE'].value
        self._SET_FLIGHT_VERIFICATION_CODE = self._database.constants['SET_FLIGHT_VERIFICATION_CODE'].value
        self._CLEAR_LOGS_VERIFICATION_CODE = self._database.constants['CLEAR_LOGS_VERIFICATION_CODE'].value

    @property
    def haveResetRequest(self) -> bool:
        """
        :return: Whether a telecommand requesting a chip reset has been received.
        """
        return self._haveResetRequest

    def run(self):
        """ Handle telecommands. """
        while not self._stopEvent.is_set():
            for telecommand in self._rxsm.receiveTelecommands(errorHandler=self._onTelecommandReceiveError):
                telecommandType = self._database.getTelecommand(telecommand.type)
                if telecommandType.isDebug and self._config.flightMode:
                    self._logger.warning(f'Ignoring received debug command {telecommand.type.name}, '
                                         f'because flight mode is enabled')
                    continue
                try:
                    handler = self._telecommandHandlers[telecommand.type]
                except KeyError:
                    self._logger.warning(f'Unimplemented command {telecommand.type.name}')
                    continue
                try:
                    response = handler(**{name.replace(' ', '_'): value for name, value in telecommand.data.items()})
                except Exception as error:
                    self._logger.error(f'Error while handling telecommand {telecommand.type.name}: {error}')
                    continue
                if telecommandType.response is not None:
                    self._rxsm.sendTelecommandResponse(telecommand, response)
                else:
                    self._rxsm.sendTelecommandAcknowledgement(telecommand)

    def _onTelecommandReceiveError(self, error):
        """
        Handle an error during receiving of a telecommand.

        :param error: The error that occurred.
        """
        self._logger.error(f'Error while receiving telecommand: {error}')

    @staticmethod
    def _handleGetStorageSpace() -> int:
        """
        Handle a GET_STORAGE_SPACE telecommand.
        Get the remaining space on the SD-card.

        :return: The remaining space on the SD-Card in bytes.
        """
        filesystemInfo = os.statvfs('/')
        return filesystemInfo.f_bavail * filesystemInfo.f_frsize

    def _handleSetFlight(self, verification_code: int, enable: bool):
        """
        Handle a SET_FLIGHT telecommand.
        Set the condition that disables debugging capabilities of the system.

        :param verification_code: A code that needs to match the predefined value to confirm the change.
        :param enable: Whether flight mode should be enabled.
        """
        if verification_code != self._SET_FLIGHT_VERIFICATION_CODE:
            self._logger.warning(f'Ignoring SET_FLIGHT command, because the verification code is invalid: '
                                 f'{verification_code!r} != {self._SET_FLIGHT_VERIFICATION_CODE!r}')
            return
        self._config.flightMode = enable
        try:
            self._config.save()
        except IOError as error:
            self._logger.error(f'Failed to save flight mode in config file: {error}')

    def _handleGetFlight(self) -> bool:
        """
        Handle a GET_FLIGHT telecommand.
        Get the flight condition state.

        :return: Whether the flight mode is enabled.
        """
        return self._config.flightMode

    def _handleSetConfig(self, config: Enum, value: Any):
        """
        Handle a SET_CONFIG telecommand.
        Set a configuration parameter.

        :param config: The configuration that should be changed.
        :param value: The new value of the configuration.
        """
        self._config[config.name] = value
        try:
            self._config.save()
        except IOError as error:
            self._logger.error(f'Failed to save config file: {error}')

    def _handleGetConfig(self, config: Enum) -> Any:
        """
        Handle a GET_CONFIG telecommand.
        Request the value of a configuration item from the experiment.

        :param config: The configuration whose value is requested.
        :return: The value of the requested configuration item.
        """
        return self._config[config.name]

    def _handleClearLogs(self, verification_code: int):
        """
        Handle a CLEAR_LOGS telecommand.
        Clear all log files on the SD-Card.

        :param verification_code: A code that needs to match the predefined value for the logs to get cleared.
        """
        if verification_code != self._CLEAR_LOGS_VERIFICATION_CODE:
            self._logger.warning(f'Ignoring CLEAR_LOGS command, because the verification code is invalid: '
                                 f'{verification_code!r} != {self._CLEAR_LOGS_VERIFICATION_CODE!r}')
            return
        self._logger.info('Clearing logs')
        logPaths = [
            '/home/pi/logs',
            '/home/pi/Videos',
        ]
        for logPath in logPaths:
            shutil.rmtree(logPath, onerror=lambda function, path, error: self._logger.error(
                f'({function}) failed to remove {path}: {error}'))
        self._haveResetRequest = True

    def _handleResetChip(self, verification_code: int):
        """
        Handle a RESET_CHIP telecommand.
        Initiate a reset of the system.

        :param verification_code: A code that needs to match the predefined value for the chip to get reset.
        """
        if verification_code != self._RESET_CHIP_VERIFICATION_CODE:
            self._logger.warning(f'Ignoring RESET_CHIP command, because the verification code is invalid: '
                                 f'{verification_code!r} != {self._RESET_CHIP_VERIFICATION_CODE!r}')
            return
        self._haveResetRequest = True

    def _handleSendCommand(self, command: str, **_) -> int:
        """
        Handle a SEND_COMMAND telecommand.
        Execute a shell command on board.

        :param command: The shell command that will be executed.
        :param _: This is the unused: The size of the command in bytes.
        :return: The exit code of the executed shell commands.
        """
        try:
            result = subprocess.run(command, shell=True, capture_output=True, encoding='utf-8', timeout=60)
        except subprocess.TimeoutExpired:
            self._logger.error(f'Command {command!r} timed out')
            return 1
        if result.stdout:
            self._logger.info(f'Output for command {command!r}:')
            for line in result.stdout.split('\n'):
                if line:
                    self._logger.info(line)
        if result.stderr:
            self._logger.error(f'Error output for command {command!r}:')
            for line in result.stderr.split('\n'):
                if line:
                    self._logger.error(line)
        return result.returncode
