import time

from gpiozero import LED

from config import Config
from threadutils import RestartableThread


class MorseBlinkInst(RestartableThread):
    def __init__(self, config: Config):
        super().__init__()
        self._config = config
        self.MorseBlinkPin = None
        self.MorseBlink_Proc_Ini()

    def MorseBlink_Proc_Ini(self):
        if not self._config.morseBlinkEnabled:
            return

        self.MorseBlinkPin = LED(self._config.morseBlinkPin)
        self.MorseBlinkPin.off()

    def run(self):
        if not self._config.morseBlinkEnabled:
            return
        while not self._stopEvent.is_set():
            self.MorseBlinkPin.on()
            time.sleep(0.5)
            self.MorseBlinkPin.off()
            time.sleep(0.5)
