import os

from time import time, strftime

from gpiozero import CPUTemperature

from config import Config
from timeutils import sleepUntil
from threadutils import RestartableThread


class CpuTemperatureReaderThread(RestartableThread):
    """ A thread that reads the CPU temperature and writes it into a CSV file. """

    def __init__(self, config: Config):
        super().__init__()
        self._config = config
        self._outputPath = '/home/pi/logs/temperature.csv'
        try:
            os.makedirs(os.path.dirname(self._outputPath), exist_ok=True)
        except IOError as error:
            self._logger.error(f'Failed to ensure the CPU temperature log save path at {self._outputPath}, '
                               f'saving CPU temperature data might not work: {error}')
        self._cpuTemperatureSensor = CPUTemperature()
        self._currentTemperature = self._cpuTemperatureSensor.temperature

    @property
    def currentTemperature(self) -> float:
        """
        :return: The last measured CPU temperature in degree Celsius.
        """
        return self._currentTemperature

    def run(self):
        if not self._config.cpuSensorEnabled:
            return
        while not self._stopEvent.is_set():
            tick = time()
            with open(self._outputPath, 'a') as log:
                self._currentTemperature = self._cpuTemperatureSensor.temperature
                log.write(f'{strftime("%Y-%m-%d %H:%M:%S")},{self._currentTemperature}\n')
            sleepUntil(tick + self._config.cpuTemperatureCheckInterval, sleepFunction=self._stopEvent.wait)
