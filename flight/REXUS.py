import os
import sys
import logging

from typing import Dict, Any, Callable, Iterable, Union
from threading import Lock

import gpiozero
import serial

from config import Config

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.parser import TelecommandParser, ParserError
    from ecom.message import TelemetryType, Telecommand
    from ecom.checksum import ChecksumVerifier
    from ecom.database import CommunicationDatabase
    from ecom.response import VariableSizedResponseTelemetrySerializer
except ImportError:
    raise ImportError('Shared communication database module missing, '
                      'run "git submodule update --init --recursive"')


class REXUSInst:
    def __init__(self, config: Config):
        self._config = config
        self.LOPin = gpiozero.Button(pin=self._config.rxsLoPin, pull_up=self._config.rxsLoPinPullUp,
                                     bounce_time=self._config.rxsLoPinDebounceTime)
        self.SODSPin = gpiozero.Button(pin=self._config.rxsSodsPin, pull_up=self._config.rxsSodsPinPullUp,
                                       bounce_time=self._config.rxsSodsPinDebounceTime)
        self.SOEPin = gpiozero.Button(pin=self._config.rxsSoePin, pull_up=self._config.rxsSoePinPullUp,
                                      bounce_time=self._config.rxsSoePinDebounceTime)

        logging.debug(f'REXUS LO signal initial read value: {self.LOPin.is_pressed}')
        logging.debug(f'REXUS SODS signal initial read value: {self.SODSPin.is_pressed}')
        logging.debug(f'REXUS SOE signal initial read value: {self.SOEPin.is_pressed}')

    def REXUS_Proc(self):
        return self.LOPin.is_pressed, self.SODSPin.is_pressed, self.SOEPin.is_pressed


class Rxsm:
    def __init__(self, database: CommunicationDatabase):
        """
        Initialize and connect to the RXSM connection.

        :param database: The communication database to use for communication.
        """
        # Initialise any variables
        self._database = database
        self._serializer = VariableSizedResponseTelemetrySerializer(
            self._database, verifier=ChecksumVerifier(self._database))
        self._parser = TelecommandParser(self._database, verifier=ChecksumVerifier(self._database))
        self._connection = serial.Serial("/dev/ttyAMA0", 38400, timeout=0.5)
        self._sendMutex = Lock()
        self._reconnectMutex = Lock()
        self._ACKNOWLEDGE_TELEMETRY = database.getTelemetryByName('ACKNOWLEDGE')
        self._TELEMETRY_MAX_RETRIES = 5

    def receiveTelecommands(self, errorHandler: Callable[[Union[ParserError, IOError]], None]) -> Iterable[Telecommand]:
        """
        Block and receive telecommands from the groundstation.

        :param errorHandler: A handler that will be called when an error occurred during receiving
        :return: An iterator over all parsed telecommands.
        """
        try:
            buffer = self._connection.read(self._connection.inWaiting() or 1)
        except IOError as error:
            errorHandler(error)
            self._reconnect()
            return
        if buffer:
            yield from self._parser.parse(buffer, errorHandler=errorHandler)

    def sendTelecommandResponse(self, telecommand: Telecommand, value: Any):
        """
        Send a telecommand response package via the RXSM connection to the groundstation.

        :param telecommand: The telecommand that this response refers to.
        :param value: The response value of the telecommand.
        """
        self._writeToRxsm(self._serializer.serializeTelecommandResponse(telecommand, value))

    def sendTelecommandAcknowledgement(self, telecommand: Telecommand):
        """
        Send an acknowledgement for a telecommand.

        :param telecommand: The telecommand to acknowledge.
        """
        self._writeToRxsm(self._serializer.serializeTelecommandAcknowledge(telecommand))

    def sendTelemetry(self, telemetryType: TelemetryType, data: Dict[str, Any]):
        """
        Send a telemetry package via the RXSM connection to the groundstation.

        :param telemetryType: The type of telemetry to send.
        :param data: The data for the telemetry.
        """
        self._writeToRxsm(self._serializer.serialize(telemetryType, **data))

    def _writeToRxsm(self, data: bytes):
        """
        Write the data to Rxsm.
        This function is thread save and can be called from different threads at the same time.

        :param data: The data to send.
        """
        with self._sendMutex:
            for _ in range(self._TELEMETRY_MAX_RETRIES):
                try:
                    self._connection.write(data)
                    break
                except IOError:
                    self._reconnect()

    def _reconnect(self):
        """ Reconnect to the RXSM serial connection. """
        if not self._reconnectMutex.acquire(blocking=False):
            with self._reconnectMutex:
                pass  # Just wait for the other thread to reconnect
            return
        try:
            try:
                self._connection.close()
            except IOError:
                pass
            try:
                self._connection.open()
            except IOError:
                pass  # Reconnection failed, but we shouldn't log about it
        finally:
            self._reconnectMutex.release()


if __name__ == "__main__":
    _database = CommunicationDatabase(os.path.join(os.path.dirname(__file__), 'communication'))
    REXUS_Inst = REXUSInst(Config.load(_database))
    _MESSAGE_TELEMETRY = _database.getTelemetryByName('MESSAGE')
    Downlink_Inst = Rxsm(_database)
    import time
    k = 0
    while True:
        LOState, SODState, SOEState = REXUS_Inst.REXUS_Proc()
        _message = f'LOState = {LOState}, SODState = {SODState}, SOEState = {SOEState}'
        _encodedMessage = _message.encode('utf-8')
        Downlink_Inst.sendTelemetry(
            _MESSAGE_TELEMETRY, {'time': k, 'type': 0, 'size': len(_encodedMessage), 'message': _encodedMessage})
        print(_message)
        time.sleep(0.5)
        k += 1
        if k >= 255:
            break
