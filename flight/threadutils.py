import logging

from abc import ABC, abstractmethod

from threading import Thread, Event


class RestartableThread(ABC):
    """
    A thread that can be stopped and restarted.
    Any subclass must check `self._stopEvent.is_set()` in its `run` function.
    """
    def __init__(self):
        super().__init__()
        self._logger = logging.getLogger(self.__class__.__name__)
        self._stopEvent = Event()
        self._thread = None

    def start(self):
        """ Start the thread. If the thread is already running, only a warning is printed. """
        if self._thread is not None:
            self._logger.warning(f'Called {self.__class__.__name__}.start, but the thread was already started')
            return
        self._stopEvent.clear()
        self._thread = Thread(target=self.run)
        self._thread.start()

    def stop(self):
        """ Stop this thread and wait up to 5 seconds for it. """
        self._stopEvent.set()
        if self._thread is None:
            return
        try:
            self._thread.join(timeout=5)
        except TimeoutError:
            self._logger.error(f'Failed to stop thread {self.__class__.__name__}')
        self._thread = None

    @property
    def running(self) -> bool:
        """
        :return: Whether the thread is currently running.
        """
        return self._thread is not None and self._thread.is_alive()

    @abstractmethod
    def run(self):
        """ The function that gets run on the thread. """
