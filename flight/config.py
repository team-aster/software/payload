import os
import sys
import json
import logging

from enum import Enum
from typing import List

try:
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), 'communication', 'ECom')))
    from ecom.database import CommunicationDatabase
    from ecom.datatypes import StructType, structDataclass, structField
except ImportError:
    raise ImportError('Shared communication database module missing, '
                      'run "git submodule update --init --recursive"')

_CONFIG_FILE_PATH = os.path.join(os.path.dirname(__file__), 'config.json')

second = float
rpm = float
ampere = float
ohm = float
hertz = int


class Resolution(StructType):
    """ A resolution of an image. """
    width: int
    """ The width of the image in pixel. """

    height: int
    """ The height of the image in pixel. """


class SetPoint(StructType):
    """ A target set-point for the motor controller. """
    deltaTime: int = structField(name='delta time')
    """ The time delta in seconds to the next set-point. """

    targetDutyCycle: int = structField(name='target duty cycle')
    """ The target duty cycle of the motor controller where 0 is 0% and 255 is 100%. """


class ConfigJsonEncoder(json.JSONEncoder):
    """ A json encoder that allows writing the configuration to disk. """
    def default(self, x):
        if isinstance(x, bytes):
            return x.decode('utf-8')
        if isinstance(x, Enum):
            return x.name
        return super().default(x)


class Config(StructType):
    """ A class that provides access to the configuration of the payload. """

    flightMode: bool = structField(name='flight mode')
    mainThreadFrequency: float = structField(name='main thread frequency')
    reactionWheelEnabled: bool = structField(name='reaction wheel enabled')
    reactionWheelEnablePin: int = structField(name='reaction wheel enable pin')
    reactionWheelSpeedControlPin: int = structField(name='reaction wheel speed control pin')
    reactionWheelSpeedFeedbackAdcChannel: int = structField(name='reaction wheel speed feedback ADC channel')
    reactionWheelSpeedFeedbackAdcDevice: int = structField(name='reaction wheel speed feedback ADC device')
    reactionWheelCurrentFeedbackAdcChannel: int = structField(name='reaction wheel current feedback ADC channel')
    reactionWheelCurrentFeedbackAdcDevice: int = structField(name='reaction wheel current feedback ADC device')
    reactionWheelRamp: List[SetPoint] = structField(
        name='reaction wheel ramp', typ=lambda database: database.parseKnownTypeInfo('SetPoint[28]'))
    reactionWheelMaxSpeed: rpm = structField(name='reaction wheel max speed')
    reactionWheelSpeedControlPinFrequency: hertz = structField(name='reaction wheel speed control pin frequency')
    reactionWheelThreadFrequency: second = structField(name='reaction wheel thread frequency')
    videoSystemEnabled: bool = structField(name='video system enabled')
    videoFps: int = structField(name='video FPS')
    videoResolution: Resolution = structField(name='video resolution')
    videoChunkLength: second = structField(name='video chunk length')
    rxsSodsPin: int = structField(name='RXS SODS pin')
    rxsSoePin: int = structField(name='RXS SOE pin')
    rxsLoPin: int = structField(name='RXS LO pin')
    rxsLoPinPullUp: bool = structField(name='RXS LO pin pull up')
    rxsSodsPinPullUp: bool = structField(name='RXS SODS pin pull up')
    rxsSoePinPullUp: bool = structField(name='RXS SOE pin pull up')
    rxsLoPinDebounceTime: second = structField(name='RXS LO pin debounce time')
    rxsSodsPinDebounceTime: second = structField(name='RXS SODS pin debounce time')
    rxsSoePinDebounceTime: second = structField(name='RXS SOE pin debounce time')
    continuityAdcDevice: int = structField(name='continuity ADC device')
    continuityAdcChannel: int = structField(name='continuity ADC channel')
    continuityComparingResistance: ohm = structField(name='continuity comparing resistance')
    continuityCurrentLimitingResistance: ampere = structField(name='continuity current limiting resistance')
    continuityLogPath: bytes = structField(
        name='continuity log path', typ=lambda database: database.parseKnownTypeInfo('char[57]'))
    continuityThreadFrequency: second = structField(name='continuity thread frequency')
    cpuTemperatureCheckInterval: second = structField(name='CPU temperature check interval')
    cpuSensorEnabled: bool = structField(name='CPU sensor enabled')
    goProEnabled: bool = structField(name='GoPro enabled')
    goProTriggerPin: int = structField(name='GoPro trigger pin')
    goProActivationPressDuration: second = structField(name='GoPro activation press duration')
    goProDeactivationPressDuration: second = structField(name='GoPro deactivation press duration')
    morseBlinkEnabled: bool = structField(name='morse blink enabled')
    morseBlinkPin: int = structField(name='morse blink pin')
    heartbeatSendInterval: second = structField(name='heartbeat send interval')
    telemetrySendInterval: second = structField(name='telemetry send interval')
    enableVerboseLogging: bool = structField(name='enable verbose logging')

    # Note: This is just a mapping, when adding something here
    # it must also be added to communication/configuration.csv

    @classmethod
    def load(cls, database: CommunicationDatabase) -> 'Config':
        """
        Load the configuration from the SD-Card.
        If loading fails, load the default configuration.

        :return: The loaded configuration.
        """
        logger = logging.getLogger(cls.__name__)
        # noinspection PyGlobalUndefined
        global Resolution, SetPoint, Config
        Resolution = structDataclass(database, replaceType=True)(Resolution)
        SetPoint = structDataclass(database, replaceType=True)(SetPoint)
        Config = structDataclass(database, replaceType='Configuration')(cls)
        configData = {}
        try:
            with open(_CONFIG_FILE_PATH) as configFile:
                configData = json.load(configFile)
            logger.info('Config file read successfully')
            return Config(configData)
        except (IOError, json.JSONDecodeError, TypeError, ValueError) as error:
            logger.error(f'Failed to load config file: {error}')
            if not configData:
                logger.error('Will be using default config values')
            else:
                logger.error('Will be using partial default config values')
        return Config(database.constants['DEFAULT_CONFIGURATION'].value)

    def save(self):
        """ Save the current configuration values to the config file. """
        with open(_CONFIG_FILE_PATH, 'w') as configFile:
            json.dump(dict(self), configFile, ensure_ascii=False, indent=2, cls=ConfigJsonEncoder)


if __name__ == '__main__':
    testDatabase = CommunicationDatabase('communication')
    testConfiguration = Config.load(testDatabase)
    print(testConfiguration)
    testConfiguration.save()
